<?php


namespace Wunder\Esorm\Eloquent;

/**
 * Trait MustNot
 * @package Wunder\Esorm\Eloquent
 */
trait MustNot
{


    public function newMustNot($type, $key, $value)
    {
        if($type instanceof Closure ){
            $bu = new Builder();
            $builder = $condition($bu);
            $this->mustNot()->where($builder->getQuery());
        }else{
            $this->mustNot()->where($type, $key, $value);
        }
        return $this;
    }

    public function mustNotTerm($key, $value="")
    {
        $this->mustNot()->term( $key, $value);
        return $this;
    }

    public function mustNotTerms($key, $value=[])
    {
        $value = (array)$value;
        $this->mustNot()->terms($key, $value);
        return $this;
    }

    public function mustNotMatch(string $key, string $value)
    {
        $this->mustNot()->match($key, $value);
        return $this;
    }
    public function mustNotMatchPhrase(string $key, string $value)
    {
        $this->mustNot()->matchPhrase($key, $value);
        return $this;
    }

    public function mustNotRange($key, array $value)
    {
        $this->mustNot()->range($key, $value);
        return $this;
    }

}
